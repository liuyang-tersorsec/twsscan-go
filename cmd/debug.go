//+build debug

package main

import (
	"crypto/sha1"
	b64 "encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	twsscan "gitlab.com/liuyang-tersorsec/twsscan-go"
)

var (
	base64 bool
	hash   bool
)

func init() {
	flag.BoolVar(&base64, "b", false, "base64")
	flag.BoolVar(&hash, "h", false, "hash")
}

func work() {
	path := "../webshell-file"
	flag.Parse()

	if !base64 && !hash {
		flag.Usage()
		return
	}

	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if strings.HasPrefix(path, "README") {
			return nil
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return nil
		}

		if len(b) == 0 {
			return nil
		}

		if base64 {
			file := twsscan.ExtractFile(b)
			fmt.Printf("%s:%s\n", info.Name(), b64.StdEncoding.EncodeToString(file))
		} else {
			h := sha1.Sum(b)
			fmt.Printf("%s:%x\n", info.Name(), h)
		}

		return nil
	})
}
