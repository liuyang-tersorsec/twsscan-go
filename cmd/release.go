// +build !debug

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"

	twsscan "gitlab.com/liuyang-tersorsec/twsscan-go"
)

var printTmp = `{
	"scan-type": "%s",
	"score": %f,
	"filename": "%s"
}
`

var (
	filename string
	content  string
)

func init() {
	flag.StringVar(&filename, "f", "", "filename")
	flag.StringVar(&content, "c", "", "content")
}

func work() {
	flag.Parse()

	if filename == "" && content == "" {
		flag.Usage()
		return
	}

	c := twsscan.StringToBytes(content)
	var err error

	if len(c) == 0 {
		c, err = ioutil.ReadFile(filename)
		if err != nil {
			log.Fatalf("open file <%s> failed, err: %s\n", filename, err.Error())
		}
	}

	result, err := twsscan.Scan(c)
	if err != nil {
		log.Fatalf("scan failed, err: %s\n", err.Error())
	}

	fmt.Printf(printTmp, result.Type(), result.Score(), result.FileName())
}
